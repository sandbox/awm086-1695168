<?php

/*
 * @file
 * Watson Transcription Classes 
 * 
 */



class WatsonTranscriptionAuthentication implements HttpClientAuthentication {

  /**
   * This is a little utility just to add our authentication to the header.
   * Currently works with OAuth V1 which is using Using the Client Credentials
   * @See https://devconnect-api.att.com/docs/oauth-v1/obtain-access-token
   *
   * @param HttpClientRequest $request
   * @return void
   * 
   */
  public function authenticate($request) {
    
    $api_key = variable_get('watson_transcription_api_key');
    $secret_key = variable_get('watson_transcription_secret_key');
    
    if (is_null($api_key)) {
      throw new Exception('No API Key');
    }
    if (is_null($secret_key)) {
      throw new Exception('No Secret Key');
    }
    
    $scope = variable_get('watson_transcription_scope', 'SPEECH');
    $full_token = variable_get('watson_transcription_full_token', FALSE);
    
    // Check to see if token is expired. Note the documentation does not specify
    // Whethere the token expires or not. 
    // Currently the token does not expire according to ATT documentation.
    if (!empty($full_token['access_token'])) {
      if ($full_token['expires_in'] == 0) {
        // It is valid, so add it to our regular request
        $request->addHeader('Authorization', 'Bearer ' . $full_token['access_token'] );
      }
    }
    else {
      // Get access token
      $formatter = new HttpClientCompositeFormatter(HttpClientBaseFormatter::FORMAT_FORM, HttpClientBaseFormatter::FORMAT_JSON);
      $client = new HttpClient(NULL, $formatter);
    
      $post_data = array(
        "client_id" => $api_key,
        "client_secret" => $secret_key,
        "scope" => $scope,
        "grant_type" => "client_credentials",
      );
    
      $url = variable_get('watson_transcription_fqdn') . variable_get('watson_transcription_oauth_endpoint');
      $request = new HttpClientRequest($url, array(
        'method' => HttpClientRequest::METHOD_POST,
        //'parameters' => $arguments,
        'data' => $post_data,
      ));
    
      try {
        $res = $client->execute($request);
        variable_set('watson_transcription_full_token', $res);
      }
      catch (Exception $e) {
        watchdog('watson_transcription', 'Failed to get an access token');
      }
    
    }

  }
}


class WatsonTranscriptionFormatter implements HttpClientFormatter {
  
  const FORMAT_AUDIO = 'audio';
  const FORMAT_JSON = 'json';
  
  private $sendFormat, $receiveFormat;
  
  protected $mimeTypes = array(
    self::FORMAT_AUDIO => 'audio/wav',
    self::FORMAT_JSON => 'application/json',
  );
  
  public function __construct($send_format = self::FORMAT_AUDIO, $receive_format = self::FORMAT_JSON) {
    $this->sendFormat = $send_format;
    $this->receiveFormat = $receive_format;
    
  }
  
  /**
   * Serializes arbitrary data to the implemented format.
   *
   * @param mixed $data
   *  The data that should be serialized.
   * @return string
   *  The serialized data as a string.
   */

  public function serialize($data) {
    switch ($this->sendFormat) {
      case self::FORMAT_AUDIO:
        return $data;
        break;
      case self::FORMAT_JSON:
        return drupal_json_encode($data);
        break;
    }
  }
  
  /**
   * Unserializes data in the implemented format.
   *
   * @param string $data
   *  The data that should be unserialized.
   * @return mixed
   *  The unserialized data.
   */
  public function unserialize($data) {
    switch ($this->receiveFormat) {
      case self::FORMAT_JSON:
        $response = drupal_json_decode($data);
        if ($response === NULL) {
          throw new Exception(t('Unserialization of response body failed.'), 1);
        }
        return $response;
        break;
    }
  }
  
  /**
   * Return the mime type that the formatter can parse.
   */
  public function accepts() {
    return $this->mimeTypes[$this->receiveFormat];
  }
  
  /**
   * Return the content type form the data the formatter generates.
   */
  public function contentType() {
    return $this->mimeTypes[$this->sendFormat];
  }
} 