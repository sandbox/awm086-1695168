<?php

/*
 * @file Admin Settings for Watson Transcription Webservices
 * 
 */


function watson_transcription_admin_settings() {

  $form = array();
  
  $form['watson_transcription_api_key'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('watson_transcription_api_key', ''),
    '#description' => 'Please Enter your api key provided to you by AT&T',
    '#title' => 'API Key',
    '#required' => TRUE,
  );
  
  $form['watson_transcription_secret_key'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('watson_transcription_secret_key', ''),
    '#description' => 'Please Enter your Secret key provided to you by AT&T',
    '#title' => 'Secret Key',
    '#required' => TRUE,
  );
  $form['watson_transcription_scope'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('watson_transcription_scope', 'SPEECH'),
    '#description' => 'Enter the scope of the api',
    '#title' => 'Scope',
    '#required' => TRUE,
  );
  
  $form['watson_transcription_fqdn'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('watson_transcription_fqdn', 'https://api.att.com/'),
    '#description' => 'Fully qualified domain name. For example, ("https://api.att.com/").',
    '#title' => 'FQDN',
    '#required' => TRUE,
  );
  
  $form['watson_transcription_service_endpoint'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('watson_transcription_service_endpoint', 'rest/1/SpeechToText'),
    '#description' => 'Please enter the service endpoint. For example, ("rest/1/SpeechToText").',
    '#title' => 'Service Endpoint',
    '#required' => TRUE,
  );
  
  $form['watson_transcription_oauth_endpoint'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('watson_transcription_oauth_endpoint', 'oauth/token'),
    '#description' => 'Please Enter the Ouath Endpoint ',
    '#title' => 'Oauth Endpoint',
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
